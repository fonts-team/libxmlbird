libxmlbird (1.2.15-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Drop build-dependency on python3-pkg-resources.
    This build-dependency is long unused. (Closes: #1083465)
  * debian/control: Drop versioned build dependency of valac.
  * debian/control: Bump Standards-Version to 4.7.0.

 -- Boyuan Yang <byang@debian.org>  Fri, 04 Oct 2024 11:51:14 -0400

libxmlbird (1.2.15-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062734

 -- Steve Langasek <vorlon@debian.org>  Fri, 01 Mar 2024 07:19:55 +0000

libxmlbird (1.2.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Thu, 11 Jan 2024 23:17:09 -0500

libxmlbird (1.2.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/rules:
    + Use Makefile files under /usr/share/dpkg/ instead of manual
      invocation of dpkg-architecture.
    + Pass Debian global CFLAGS and LDFLAGS to Vala build.
    + Enable full hardening.

 -- Boyuan Yang <byang@debian.org>  Tue, 28 Nov 2023 10:06:25 -0500

libxmlbird (1.2.12-2) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.2.
  * debian/clean: Clean generated build files properly. (Closes: #1048077)

 -- Boyuan Yang <byang@debian.org>  Fri, 18 Aug 2023 22:24:35 -0400

libxmlbird (1.2.12-1) unstable; urgency=medium

  * New upstream release
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Hideki Yamane <henrich@debian.org>  Mon, 21 Sep 2020 19:40:46 +0900

libxmlbird (1.2.11-3) unstable; urgency=medium

  * debian/control
    - Set Multi-Arch: same for libxmlbird-dev

 -- Hideki Yamane <henrich@debian.org>  Sat, 27 Jun 2020 18:06:06 +0900

libxmlbird (1.2.11-2) unstable; urgency=medium

  * Trim trailing whitespace.
  * Update standards version to 4.5.0, no changes needed.

 -- Hideki Yamane <henrich@debian.org>  Sun, 24 May 2020 07:28:30 +0900

libxmlbird (1.2.11-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 4.4.1

 -- Hideki Yamane <henrich@debian.org>  Mon, 02 Dec 2019 19:45:30 +0900

libxmlbird (1.2.10-2) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.4.0
    - use dh12
    - add Rules-Requires-Root: no
  * drop debian/compat
  * debian/copyright
    - use https
    - update copyright year
  * add debian/salsa-ci.yml

 -- Hideki Yamane <henrich@debian.org>  Sun, 29 Sep 2019 15:16:23 +0900

libxmlbird (1.2.10-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 20 Oct 2018 08:55:02 +0900

libxmlbird (1.2.9-2) unstable; urgency=medium

  * debian/control
    - update Vcs-* to move salsa.debian.org
    - set Standards-Version: 4.2.1
    - update maintainer address

 -- Hideki Yamane <henrich@debian.org>  Sat, 22 Sep 2018 15:34:18 +0900

libxmlbird (1.2.9-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Standards-Version: 4.1.3
    - Depends: debhelper (>= 11)
  * debian/compat
    - set 11
  * debian/patches
    - drop reproducible_build.patch, merged upstream

 -- Hideki Yamane <henrich@debian.org>  Sun, 07 Jan 2018 16:02:43 +0900

libxmlbird (1.2.7-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - update copyright year

 -- Hideki Yamane <henrich@debian.org>  Tue, 28 Nov 2017 19:48:05 +0900

libxmlbird (1.2.6-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - adjust python3-doit version
    - set Standards-Version: 4.1.1
    - set Multi-Arch: same for libxmlbird1
  * debian/watch
    - use https
    - update to version 4
  * debian/upstream
    - automatically converted .pgp to .asc
  * debian/rules
    - deal with upstream's change in install.py

 -- Hideki Yamane <henrich@debian.org>  Sat, 04 Nov 2017 08:47:08 +0900

libxmlbird (1.2.4-3) unstable; urgency=medium

  * debian/rules
    - deal with doit package's change (Closes: #871206)
  * debian/control
    - set Standards-Version: 4.0.1 with no change

 -- Hideki Yamane <henrich@debian.org>  Thu, 17 Aug 2017 21:26:11 +0900

libxmlbird (1.2.4-2) unstable; urgency=medium

  * debian/patches
    - add reproducible_build.patch (Closes: #828122)
      Thanks to Chris Lamb <lamby@debian.org>

 -- Hideki Yamane <henrich@debian.org>  Tue, 14 Feb 2017 07:33:07 +0900

libxmlbird (1.2.4-1) unstable; urgency=medium

  * New upstream release
  * use debhelper 10
    - drop unnecessary Build-Depends: autotools-dev

 -- Hideki Yamane <henrich@debian.org>  Mon, 10 Oct 2016 02:43:22 +0900

libxmlbird (1.2.2-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 3.9.8
    - use https for Upstream URL
  * update debian/gbp.conf

 -- Hideki Yamane <henrich@debian.org>  Sun, 19 Jun 2016 09:24:54 +0900

libxmlbird (1.2.0-1) unstable; urgency=medium

  * New upstream release
  * debian/patches
    - drop 0001-don-t-forget-libxmlbird_pkgconfig.patch, merged upstream

 -- Hideki Yamane <henrich@debian.org>  Fri, 15 Jan 2016 21:39:33 +0900

libxmlbird (1.0.7-2) unstable; urgency=medium

  * debian/control
    - add Vcs-*
  * add debian/gbp.conf for git-buildpackage

 -- Hideki Yamane <henrich@debian.org>  Mon, 28 Dec 2015 20:15:13 +0900

libxmlbird (1.0.7-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - adjust Build-Depends: python3-doit (>= 0.20), python3-pkg-resources for
      python3
  * debian/rules
    - use doit3 instead of doit for python3
  * debian/patches
    - add 0001-don-t-forget-libxmlbird_pkgconfig.patch for missing pkgconfig
      file

 -- Hideki Yamane <henrich@debian.org>  Sun, 15 Nov 2015 13:47:40 +0900

libxmlbird (1.0.6-1) unstable; urgency=medium

  * Imported Upstream version 1.0.6
  * drop debian/patches since merged into upstream

 -- Hideki Yamane <henrich@debian.org>  Tue, 30 Jun 2015 07:25:37 +0900

libxmlbird (1.0.5-1) unstable; urgency=medium

  * Imported Upstream version 1.0.5

 -- Hideki Yamane <henrich@debian.org>  Tue, 30 Jun 2015 06:25:16 +0900

libxmlbird (1.0.4-3) unstable; urgency=medium

  * debian/control
    - add python-pkg-resources to fix FTBFS with newer python-doit
    - set Debian Fonts Task Force <pkg-fonts-devel@lists.alioth.debian.org> as
      Maintainer
  * add debian/patches/0001-fix-build-failure-on-kfreeBSD.patch

 -- Hideki Yamane <henrich@debian.org>  Mon, 29 Jun 2015 22:16:26 +0900

libxmlbird (1.0.4-2) unstable; urgency=medium

  * Upload to unstable (Closes: #789171)

 -- Hideki Yamane <henrich@debian.org>  Fri, 19 Jun 2015 00:01:45 +0900

libxmlbird (1.0.4-1) vivid; urgency=medium

  * New upstream release

 -- Johan Mattsson <johan.mattsson.m@gmail.com>  Wed, 03 Jun 2015 15:16:36 +0200

libxmlbird (1.0.3-1) vivid; urgency=medium

  * Build dependencies

 -- Johan Mattsson <johan.mattsson.m@gmail.com>  Tue, 02 Jun 2015 02:37:43 +0200

libxmlbird (1.0.3-1) vivid; urgency=medium

  * New upstream release

 -- Johan Mattsson <johan.mattsson.m@gmail.com>  Tue, 02 Jun 2015 02:23:55 +0200

libxmlbird (1.0.2-1) unstable; urgency=low

  * Initial release

 -- Johan Mattsson <johan.mattsson.m@gmail.com>  Tue, 02 Jun 2015 01:15:13 +0200
